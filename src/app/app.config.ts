import { ApplicationConfig, isDevMode } from '@angular/core';
import { provideRouter } from '@angular/router';

import { routes } from './app.routes';
import { provideClientHydration } from '@angular/platform-browser';
import { provideAnimationsAsync } from '@angular/platform-browser/animations/async';
import { provideState, provideStore } from '@ngrx/store';
import { provideEffects } from '@ngrx/effects';
import { provideStoreDevtools } from '@ngrx/store-devtools';
import * as fromCollaborateur from './store/reducers/collaborateur.reducer';
import * as fromEvents from './store/reducers/event.reducer';
import { collaborateursFeatureKey } from './store/reducers/collaborateur.reducer';
import { CollaborateurEffects } from './store/effects/collaborateur.effects';
import { MAT_DATE_LOCALE, provideNativeDateAdapter } from '@angular/material/core';
import { EventEffects } from './store/effects/event.effects';
import { eventsFeatureKey } from './store/reducers/event.reducer';

export const appConfig: ApplicationConfig = {
  providers: [
    provideRouter(routes),
    provideClientHydration(),
    provideAnimationsAsync(),
    provideStore(),
    provideEffects(CollaborateurEffects),
    provideState(collaborateursFeatureKey, fromCollaborateur.reducer),
    provideEffects(EventEffects),
    provideState(eventsFeatureKey, fromEvents.reducer),
    provideStoreDevtools({
      maxAge: 25
    }),
    provideNativeDateAdapter(),
    { provide: MAT_DATE_LOCALE, useValue: 'fr-FR' },
  ]
};
