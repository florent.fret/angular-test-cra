import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { CollaborateurComponent } from './collaborateur/collaborateur.component';
import { CalendarComponent } from './calendar/calendar.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [ RouterOutlet, CollaborateurComponent, CalendarComponent ],
  templateUrl: './app.component.html',
  styleUrl: './app.component.sass'
})
export class AppComponent {
  title = 'angular-test-ggvie';
}
