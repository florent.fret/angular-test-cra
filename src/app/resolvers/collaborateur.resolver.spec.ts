import { TestBed } from '@angular/core/testing';
import { ResolveFn } from '@angular/router';

import { collaborateurResolver } from './collaborateur.resolver';

describe('collaborateurResolver', () => {
  const executeResolver: ResolveFn<boolean> = (...resolverParameters) => 
      TestBed.runInInjectionContext(() => collaborateurResolver(...resolverParameters));

  beforeEach(() => {
    TestBed.configureTestingModule({});
  });

  it('should be created', () => {
    expect(executeResolver).toBeTruthy();
  });
});
