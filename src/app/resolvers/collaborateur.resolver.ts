import { ResolveFn } from '@angular/router';
import { Store } from '@ngrx/store';
import { inject } from '@angular/core';
import { CollaborateurActions } from '../store/actions/collaborateur.actions';

export const collaborateurResolver: ResolveFn<boolean> = (route, state) => {
  const store = inject(Store);
  // store.dispatch(CollaborateurActions.loadCollaborateurs({ collaborateurs: [] }))
  return true;
};
