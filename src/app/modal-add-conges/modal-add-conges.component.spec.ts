import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalAddCongesComponent } from './modal-add-conges.component';

describe('ModalAddCongesComponent', () => {
  let component: ModalAddCongesComponent;
  let fixture: ComponentFixture<ModalAddCongesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ModalAddCongesComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ModalAddCongesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
