import { Component } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { MatDialogClose, MatDialogContent, MatDialogRef, MatDialogTitle } from '@angular/material/dialog';
import { v4 as uuidv4 } from 'uuid';
import { CollaborateurActions } from '../store/actions/collaborateur.actions';
import { selectSelectedId } from '../store/reducers/collaborateur.reducer';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInput } from '@angular/material/input';
import { MatButton } from '@angular/material/button';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MAT_DATE_LOCALE, MatNativeDateModule } from '@angular/material/core';
import { EventActions } from '../store/actions/event.actions';

@Component({
  selector: 'app-modal-add-conges',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    MatFormFieldModule,
    MatDialogTitle,
    MatDialogContent,
    MatInput,
    MatButton,
    MatDialogClose,
    MatDatepickerModule,
  ],
  providers: [],
  templateUrl: './modal-add-conges.component.html',
  styleUrl: './modal-add-conges.component.sass'
})
export class ModalAddCongesComponent {
  eventForm!: FormGroup;

  constructor(
    private fb: FormBuilder,
    private store: Store,
    public dialogRef: MatDialogRef<ModalAddCongesComponent>
  ) {
    this.initStore();
  }


  onSubmit() {
    if (this.eventForm.valid) {
      this.store.dispatch(EventActions.addEvent({ event: this.eventForm.value }));
      this.dialogRef.close();
    }
  }

  private initStore() {
    this.store.select(selectSelectedId).subscribe(collaborateurId => {
      this.eventForm = this.fb.group({
        id: [ uuidv4(), Validators.required ],
        collaborateurId: [ collaborateurId, Validators.required ],
        start: [ '', Validators.required ],
        end: [ '', Validators.required ]
      });
    })
  }
}
