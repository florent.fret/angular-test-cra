import { Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { collaborateurResolver } from './resolvers/collaborateur.resolver';

export const routes: Routes = [
  {
    path: '',
    component: AppComponent,
    resolve: {
      collaborateurs: collaborateurResolver
    }
  }
];
