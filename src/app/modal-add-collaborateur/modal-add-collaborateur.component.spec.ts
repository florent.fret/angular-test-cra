import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalAddCollaborateurComponent } from './modal-add-collaborateur.component';

describe('ModalAddCollaborateurComponent', () => {
  let component: ModalAddCollaborateurComponent;
  let fixture: ComponentFixture<ModalAddCollaborateurComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ModalAddCollaborateurComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ModalAddCollaborateurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
