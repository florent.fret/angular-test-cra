import { Component } from '@angular/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatDialogContent, MatDialogModule, MatDialogRef, MatDialogTitle } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { Store } from '@ngrx/store';
import { CollaborateurActions } from '../store/actions/collaborateur.actions';
import { v4 as uuidv4 } from 'uuid';

@Component({
  selector: 'app-modal-add-collaborateur',
  standalone: true,
  imports: [
    MatFormFieldModule,
    ReactiveFormsModule,
    MatDialogContent,
    MatDialogTitle,
    MatInputModule,
    MatButtonModule,
    MatDialogModule,
    MatDialogModule
  ],
  templateUrl: './modal-add-collaborateur.component.html',
  styleUrl: './modal-add-collaborateur.component.sass'
})
export class ModalAddCollaborateurComponent {
  collaborateurForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private store: Store,
    public dialogRef: MatDialogRef<ModalAddCollaborateurComponent>
  ) {
    this.collaborateurForm = this.fb.group({
      id: [uuidv4(), Validators.required],
      name: ['', Validators.required],
      lastName: ['', Validators.required]
    });
  }

  onSubmit() {
    if (this.collaborateurForm.valid) {
      this.store.dispatch(CollaborateurActions.addCollaborateur({ collaborateur: this.collaborateurForm.value }));
      this.dialogRef.close();
    }
  }
}
