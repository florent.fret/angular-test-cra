import { Component, OnInit } from '@angular/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { Observable } from 'rxjs';
import { Collaborateur } from '../store/models/collaborateur.model';
import { select, Store } from '@ngrx/store';
import { selectAll, selectCollaborateurById } from '../store/reducers/collaborateur.reducer';
import { CommonModule } from '@angular/common';
import { MatOptionModule } from '@angular/material/core';
import { MatButton, MatIconButton } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatDialog } from '@angular/material/dialog';
import { ModalAddCollaborateurComponent } from '../modal-add-collaborateur/modal-add-collaborateur.component';
import { MatCardModule } from '@angular/material/card';
import { MatDivider } from '@angular/material/divider';
import { ModalAddCongesComponent } from '../modal-add-conges/modal-add-conges.component';
import { CollaborateurActions } from '../store/actions/collaborateur.actions';

@Component({
  selector: 'app-collaborateur',
  standalone: true,
  imports: [
    MatFormFieldModule,
    MatSelectModule,
    MatOptionModule,
    ReactiveFormsModule,
    CommonModule,
    MatIconModule,
    MatIconButton,
    MatCardModule,
    MatDivider,
    MatButton
  ],
  templateUrl: './collaborateur.component.html',
  styleUrl: './collaborateur.component.sass'
})
export class CollaborateurComponent implements OnInit {
  collaborateurs$: Observable<Collaborateur[]>;
  collaborateurControl = new FormControl();
  collaborateurSelected$!: Observable<Collaborateur | undefined>;

  constructor(private store: Store, private dialog: MatDialog) {
    this.collaborateurs$ = this.store.pipe(select(selectAll));
    this.selectCollabSelected();
  }

  private selectCollabSelected() {
    this.collaborateurSelected$ = this.store.select(selectCollaborateurById(this.collaborateurControl.value));
    this.store.dispatch(CollaborateurActions.selectCollaborateur({ id: this.collaborateurControl.value}))
  }

  ngOnInit(): void {
    this.collaborateurControl.valueChanges.subscribe(d => {
      this.selectCollabSelected();
    })
  }

  openModalCollaborateur() {
    this.dialog.open(ModalAddCollaborateurComponent);
  }

  openModalAddConges() {
    this.dialog.open(ModalAddCongesComponent);
  }
}
