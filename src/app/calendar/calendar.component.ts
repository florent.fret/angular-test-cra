import { Component } from '@angular/core';
import { CalendarOptions } from '@fullcalendar/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import { FullCalendarModule } from '@fullcalendar/angular';
import { Store } from '@ngrx/store';
import { State } from '../store';
import { CalendarOption } from '@fullcalendar/angular/private-types';
import { Observable } from 'rxjs';
import { selectEventsWithCollaborateurs } from '../store/selectors/computed.selector';
import { CommonModule } from '@angular/common';
import { map } from 'rxjs/operators';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@Component({
  selector: 'app-calendar',
  standalone: true,
  imports: [
    CommonModule,
    FullCalendarModule,
  ],
  templateUrl: './calendar.component.html',
  styleUrl: './calendar.component.sass'
})
export class CalendarComponent {
  calendarOptions: CalendarOptions = {
    plugins: [ dayGridPlugin ],
    initialView: 'dayGridMonth',
    weekends: false,
    events: [
    ],
  };

  events: CalendarOption<"events"> = [
    { title: 'Meeting', start: new Date() }
  ]

  events$: Observable<any>;



  constructor(private store: Store<State>) {
    this.events$ = this.store.select(selectEventsWithCollaborateurs).pipe(
      map((events) => {
        return events.map(event => {
          return {
            title: `${event.collaborateurName} ${event.collaborateurLastName}`,
            start: event.start,
            end: event.end
          }
        })
      })
    );
  }
}
