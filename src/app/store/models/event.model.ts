export interface Event {
  id: string;
  collaborateurId: string;
  start: Date;
  end: Date;
}
