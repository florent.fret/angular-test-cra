export interface Collaborateur {
  id: string;
  name: string;
  lastName: string;
}
