import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { Update } from '@ngrx/entity';

import { Event } from '../models/event.model';

export const EventActions = createActionGroup({
  source: 'Event/API',
  events: {
    'Load Events': props<{ events: Event[] }>(),
    'Add Event': props<{ event: Event }>(),
    'Upsert Event': props<{ event: Event }>(),
    'Add Events': props<{ events: Event[] }>(),
    'Upsert Events': props<{ events: Event[] }>(),
    'Update Event': props<{ event: Update<Event> }>(),
    'Update Events': props<{ events: Update<Event>[] }>(),
    'Delete Event': props<{ id: string }>(),
    'Delete Events': props<{ ids: string[] }>(),
    'Clear Events': emptyProps(),
  }
});
