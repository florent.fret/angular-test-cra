import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { Update } from '@ngrx/entity';

import { Collaborateur } from '../models/collaborateur.model';

export const CollaborateurActions = createActionGroup({
  source: 'Collaborateur/API',
  events: {
    'Load Collaborateurs': props<{ collaborateurs: Collaborateur[] }>(),
    'Load Collaborateurs Success': props<{ collaborateurs: Collaborateur[] }>(),
    'Load Collaborateurs Failure': props<{ error: any }>(),
    'Add Collaborateur': props<{ collaborateur: Collaborateur }>(),
    'Add Collaborateur Success': props<{ collaborateur: Collaborateur }>(),
    'Add Collaborateur failure': props<{ error: any }>(),
    'Upsert Collaborateur': props<{ collaborateur: Collaborateur }>(),
    'Add Collaborateurs': props<{ collaborateurs: Collaborateur[] }>(),
    'Upsert Collaborateurs': props<{ collaborateurs: Collaborateur[] }>(),
    'Update Collaborateur': props<{ collaborateur: Update<Collaborateur> }>(),
    'Update Collaborateurs': props<{ collaborateurs: Update<Collaborateur>[] }>(),
    'Delete Collaborateur': props<{ id: string }>(),
    'Delete Collaborateurs': props<{ ids: string[] }>(),
    'Clear Collaborateurs': emptyProps(),
    'Select Collaborateur': props<{ id: string }>(),
  }
});
