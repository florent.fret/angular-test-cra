import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { CollaborateurEffects } from './collaborateur.effects';

describe('CollaborateurEffects', () => {
  let actions$: Observable<any>;
  let effects: CollaborateurEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        CollaborateurEffects,
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.inject(CollaborateurEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
