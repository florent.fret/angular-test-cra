import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map, tap } from 'rxjs/operators';
import { CollaborateurActions } from '../actions/collaborateur.actions';
import { Collaborateur } from '../models/collaborateur.model';


@Injectable()
export class CollaborateurEffects {

  loadCollaborateurs$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(CollaborateurActions.loadCollaborateurs),
      tap(d => console.log(d)),
      map(() => CollaborateurActions.loadCollaborateursSuccess({ collaborateurs: mockCollaborateurs }))
    );
  });


  constructor(private actions$: Actions) {
  }
}

export const mockCollaborateurs: Array<Collaborateur> = [
  {
    id: '1', name: 'name1', lastName: 'lastName1'
  },
  { id: '2', name: 'name2', lastName: 'lastName2' },
  { id: '3', name: 'name3', lastName: 'lastName3' },
  { id: '4', name: 'name4', lastName: 'lastName4' }
]
