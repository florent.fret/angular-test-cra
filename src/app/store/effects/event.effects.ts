import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, concatMap } from 'rxjs/operators';
import { Observable, EMPTY, of } from 'rxjs';
import { EventActions } from '../actions/event.actions';


@Injectable()
export class EventEffects {


  constructor(private actions$: Actions) {}
}
