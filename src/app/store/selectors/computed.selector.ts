import { createSelector } from '@ngrx/store';
import { selectAll as selectAllEvents } from '../reducers/event.reducer';
import { selectEntities as selectCollaborateurEntities } from '../reducers/collaborateur.reducer';

export const selectEventsWithCollaborateurs = createSelector(
  selectAllEvents, // Sélecteur pour obtenir tous les événements
  selectCollaborateurEntities, // Sélecteur pour obtenir les entités des collaborateurs
  (events, collaborateurs) => events.map(event => ({
    ...event,
    collaborateurName: collaborateurs[event.collaborateurId]?.name,
    collaborateurLastName: collaborateurs[event.collaborateurId]?.lastName
  }))
);
