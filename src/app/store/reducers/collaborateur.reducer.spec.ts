import { reducer, initialState } from '../reducers/collaborateur.reducer';

describe('Collaborateur Reducer', () => {
  describe('unknown action', () => {
    it('should return the previous state', () => {
      const action = {} as any;

      const result = reducer(initialState, action);

      expect(result).toBe(initialState);
    });
  });
});
