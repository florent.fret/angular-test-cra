import { createFeature, createFeatureSelector, createReducer, createSelector, on, State } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { Collaborateur } from '../models/collaborateur.model';
import { CollaborateurActions } from '../actions/collaborateur.actions';

export const collaborateursFeatureKey = 'collaborateurs';

export interface CollaborateurState extends EntityState<Collaborateur> {
  selectedId: string | null;
}

export const adapter: EntityAdapter<Collaborateur> = createEntityAdapter<Collaborateur>();

export const initialState: CollaborateurState = adapter.getInitialState({
  selectedId: null
});

export const reducer = createReducer(
  initialState,
  on(CollaborateurActions.addCollaborateur,
    (state, action) => adapter.addOne(action.collaborateur, state)
  ),
  on(CollaborateurActions.upsertCollaborateur,
    (state, action) => adapter.upsertOne(action.collaborateur, state)
  ),
  on(CollaborateurActions.addCollaborateurs,
    (state, action) => adapter.addMany(action.collaborateurs, state)
  ),
  on(CollaborateurActions.upsertCollaborateurs,
    (state, action) => adapter.upsertMany(action.collaborateurs, state)
  ),
  on(CollaborateurActions.updateCollaborateur,
    (state, action) => adapter.updateOne(action.collaborateur, state)
  ),
  on(CollaborateurActions.updateCollaborateurs,
    (state, action) => adapter.updateMany(action.collaborateurs, state)
  ),
  on(CollaborateurActions.deleteCollaborateur,
    (state, action) => adapter.removeOne(action.id, state)
  ),
  on(CollaborateurActions.deleteCollaborateurs,
    (state, action) => adapter.removeMany(action.ids, state)
  ),
  on(CollaborateurActions.loadCollaborateursSuccess,
    (state, action) => adapter.setAll(action.collaborateurs, state)
  ),
  on(CollaborateurActions.clearCollaborateurs,
    state => adapter.removeAll(state)
  ),
  on(CollaborateurActions.selectCollaborateur,
    (state, { id}) => ({
      ...state,
      selectedId: id
    })
  ),
);

export const collaborateursFeature = createFeature({
  name: collaborateursFeatureKey,
  reducer,
  extraSelectors: ({ selectCollaborateursState }) => ({
    ...adapter.getSelectors(selectCollaborateursState)
  }),
});

export const selectCollaborateurById = (id: string) => createSelector(
  selectEntities,
  (entities) => entities[id]
);

export const selectCollaborateursState = createFeatureSelector<CollaborateurState>(collaborateursFeatureKey);
export const selectSelectedId = createSelector(
  selectCollaborateursState,
  (state) => state.selectedId
);

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = collaborateursFeature;
